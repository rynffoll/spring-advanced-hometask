-- auditoriums
INSERT INTO auditorium (id, name, numberOfSeats) VALUES (1, 'Auditorium 1', 10);
INSERT INTO auditorium (id, name, numberOfSeats) VALUES (2, 'Auditorium 2', 40);

INSERT INTO vipSeat (auditoriumId, seat) VALUES (1, 1);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (1, 2);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (1, 3);

INSERT INTO vipSeat (auditoriumId, seat) VALUES (2, 1);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (2, 10);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (2, 20);
INSERT INTO vipSeat (auditoriumId, seat) VALUES (2, 30);

-- events
INSERT INTO event (id, name, basePrice, rating) VALUES (1, 'Grand concert', 130.0, 'MID');
INSERT INTO event (id, name, basePrice, rating) VALUES (2, 'Concert', 250.0, 'LOW');
INSERT INTO event (id, name, basePrice, rating) VALUES (3, 'The Nutcracker', 300.0, 'HIGH');

INSERT INTO seance (eventId, auditoriumId, date) VALUES (1, 1, {ts '2016-08-01 12:00:00.00'});
INSERT INTO seance (eventId, auditoriumId, date) VALUES (1, 1, {ts '2016-08-01 16:00:00.00'});
INSERT INTO seance (eventId, auditoriumId, date) VALUES (1, 2, {ts '2016-08-01 15:00:00.00'});
INSERT INTO seance (eventId, auditoriumId, date) VALUES (1, 2, {ts '2016-08-01 20:00:00.00'});

INSERT INTO seance (eventId, auditoriumId, date) VALUES (2, 1, {ts '2016-08-01 13:00:00.00'});
INSERT INTO seance (eventId, auditoriumId, date) VALUES (2, 1, {ts '2016-08-01 18:00:00.00'});
INSERT INTO seance (eventId, auditoriumId, date) VALUES (2, 2, {ts '2016-08-01 09:00:00.00'});
INSERT INTO seance (eventId, auditoriumId, date) VALUES (2, 2, {ts '2016-08-01 12:00:00.00'});
INSERT INTO seance (eventId, auditoriumId, date) VALUES (2, 2, {ts '2016-08-01 17:00:00.00'});

-- users
-- john_doe@srv.com:password
INSERT INTO user (id, firstName, lastName, email, birthday, password, roles)
VALUES (1, 'John', 'Doe', 'john@srv.com',  {ts '1970-01-01'}, '$2a$04$v6A5v8Q5gjiGOD/krun4ou7fe5Dhke4yeVMbeS58HhR.8UjOkuXDi', 'ROLE_REGISTERED_USER');
-- bob@srv.com:secret
INSERT INTO user (id, firstName, lastName, email, birthday, password, roles)
VALUES (2, 'Bob', 'Smith', 'bob@srv.com',  {ts '1970-01-01'}, '$2a$04$dbMsKtipda5emYeypkDfY.GfalERl1n9XZPOudUeX8zBgjKzvvl82', 'ROLE_REGISTERED_USER, ROLE_BOOKING_MANAGER');

-- accounts
INSERT INTO user_account (id, userId, amount) VALUES(1, 1, 0.0);
INSERT INTO user_account (id, userId, amount) VALUES(2, 2, 1000.0);
