package ua.epam.spring.hometask.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Yuriy_Tkach
 */
public class User extends DomainObject {

  private static final String ROLE_DELIMITER = ", ";

  private String firstName;
  private String lastName;
  private String email;
  private LocalDate birthday;

  @JsonIgnore
  private NavigableSet<Ticket> tickets = new TreeSet<>();

  private String password;

  private List<String> roles;

  private UserAccount userAccount;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public NavigableSet<Ticket> getTickets() {
    return tickets;
  }

  public void setTickets(NavigableSet<Ticket> tickets) {
    this.tickets = tickets;
  }

  public LocalDate getBirthday() {
    return birthday;
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday = birthday;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @JsonIgnore
  public String getRolesAsString() {
    return roles.stream().collect(Collectors.joining(ROLE_DELIMITER));
  }

  public List<String> getRoles() {
    return roles;
  }

  public void setRoles(String roles) {
    this.roles = Arrays.stream(roles.split(ROLE_DELIMITER)).collect(Collectors.toList());
  }

  public UserAccount getUserAccount() {
    return userAccount;
  }

  public void setUserAccount(UserAccount userAccount) {
    this.userAccount = userAccount;
  }

  @JsonProperty("roles")
  public void setRoles(List<String> roles) {
    this.roles = roles;
  }

  public void setRoles(String... roles) {
    this.roles = Arrays.stream(roles).collect(Collectors.toList());
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, email);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    User other = (User) obj;
    if (email == null) {
      if (other.email != null) {
        return false;
      }
    } else if (!email.equals(other.email)) {
      return false;
    }
    if (firstName == null) {
      if (other.firstName != null) {
        return false;
      }
    } else if (!firstName.equals(other.firstName)) {
      return false;
    }
    if (lastName == null) {
      if (other.lastName != null) {
        return false;
      }
    } else if (!lastName.equals(other.lastName)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "User{" +
            "firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            '}';
  }
}
