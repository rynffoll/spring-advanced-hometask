/**
 * Created by Ruslan_Kamashev on 8/3/2016.
 */
@XmlSchema(namespace = "http://localhost:8080/ws",
        elementFormDefault = XmlNsForm.QUALIFIED)
@XmlJavaTypeAdapters(value = {
        @XmlJavaTypeAdapter(value = LocalDateXmlAdapter.class, type = LocalDate.class),
        @XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class, type = LocalDateTime.class)
})
@XmlAccessorType(XmlAccessType.FIELD)
package ua.epam.spring.hometask.domain;

import com.fasterxml.jackson.core.type.TypeReference;
import com.migesok.jaxb.adapter.javatime.LocalDateTimeXmlAdapter;
import com.migesok.jaxb.adapter.javatime.LocalDateXmlAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.NavigableMap;