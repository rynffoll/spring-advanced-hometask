package ua.epam.spring.hometask.domain;

import java.math.BigDecimal;

/**
 * Created by Ruslan_Kamashev on 7/29/2016.
 */
public class UserAccount extends DomainObject {

  private BigDecimal amount;

  public UserAccount() {
  }

  public UserAccount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @Override
  public String toString() {
    return "UserAccount{" +
            "id=" + getId() + ", " +
            "amount=" + amount +
            '}';
  }
}
