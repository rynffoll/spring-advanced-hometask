package ua.epam.spring.hometask.util;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Created by rynffoll on 12.04.16.
 */
public class DateConverter {

  public static LocalDateTime toLocalDateTime(java.sql.Date date) {
    java.util.Date utilDate = new java.util.Date(date.getTime());
    return LocalDateTime.ofInstant(utilDate.toInstant(), ZoneId.systemDefault());
  }

  public static LocalDateTime toLocalDateTime(java.util.Date date) {
    return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
  }

  public static java.util.Date toDate(LocalDateTime dateTime) {
    return java.util.Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
  }

  public static java.util.Date toDate(LocalDate date) {
    return java.util.Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
  }

  public static Timestamp toTimestamp(LocalDate date) {
    return Timestamp.valueOf(date.atStartOfDay());
  }

  public static Timestamp toTimestamp(LocalDateTime dateTime) {
    return Timestamp.valueOf(dateTime);
  }
}
