package ua.epam.spring.hometask.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Created by rynffoll on 24.07.16.
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
  // for registering DelegatingFilterProxy
}
