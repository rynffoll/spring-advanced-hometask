package ua.epam.spring.hometask.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.velocity.VelocityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityLayoutViewResolver;
import ua.epam.spring.hometask.web.view.JsonViewResolver;
import ua.epam.spring.hometask.web.view.PdfViewResolver;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class ViewResolverConfig {

  @Autowired
  private JsonViewResolver jsonViewResolver;
  @Autowired
  private PdfViewResolver pdfViewResolver;

  @Bean
  @SuppressWarnings("deprecation")
  public VelocityConfigurer getVelocityConfigurer() {
    VelocityConfigurer velocityConfigurer = new VelocityConfigurer();
    velocityConfigurer.setResourceLoaderPath("/WEB-INF/html/");
    return velocityConfigurer;
  }

  /* Using HttpMessageConverter's instead of ContentNegotiatingViewResolver */
  /*
  @Bean
  @Order(0)
  public ContentNegotiatingViewResolver contentNegotiatingViewResolver() {
    List<ViewResolver> resolvers = new ArrayList<>();
    resolvers.add(jsonViewResolver);
    resolvers.add(pdfViewResolver);

    ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
    resolver.setViewResolvers(resolvers);

    return resolver;
  }
  */

  @Bean
  @Order(1)
  @SuppressWarnings("deprecation")
  public ViewResolver getVelocityLayoutViewResolver(VelocityProperties properties) {
    VelocityLayoutViewResolver velocityViewResolver = new VelocityLayoutViewResolver();
    properties.applyToViewResolver(velocityViewResolver); // for injecting content into layout
    velocityViewResolver.setLayoutUrl("layout/main.vm");
    velocityViewResolver.setPrefix("");
    velocityViewResolver.setSuffix(".vm");
    return velocityViewResolver;
  }

  @Bean
  @Order(2)
  public ViewResolver getResourceBundleViewResolver() {
    ResourceBundleViewResolver resourceBundleViewResolver = new ResourceBundleViewResolver();
    resourceBundleViewResolver.setBasename("view");
    return resourceBundleViewResolver;
  }

}
