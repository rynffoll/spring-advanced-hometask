package ua.epam.spring.hometask.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ua.epam.spring.hometask.security.Roles;
import ua.epam.spring.hometask.security.UserDetailsAuthenticationProvider;

import javax.sql.DataSource;

/**
 * Created by rynffoll on 24.07.16.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private static final int TOKEN_VALIDITY_SECONDS = 60 * 60 * 24; // 1 day
  private static final String SECRET_KEY = "My Secret Key";

  @Autowired
  private UserDetailsAuthenticationProvider authenticationProvider;

  @Autowired
  private UserDetailsService userDetailsService;

  @Autowired
  private DataSource dataSource;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
            .antMatchers("/").permitAll()
            .antMatchers("/h2/**", "/h2**", "/api/**", "/ws/**").permitAll()
            .antMatchers("/booking/**").hasRole(Roles.BOOKING_MANAGER)
            .anyRequest().hasRole(Roles.REGISTERED_USER)
            .and()
            .formLogin().loginPage("/login").defaultSuccessUrl("/").permitAll()
            .and()
            .logout().permitAll()
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) // logout and csrf
            .and()
            .rememberMe()
            .tokenRepository(tokenRepository())
            .rememberMeServices(rememberMeAuthenticationProvider())
            .tokenValiditySeconds(TOKEN_VALIDITY_SECONDS)
            .and()
            .csrf().ignoringAntMatchers("/booking/tickets"); // for loading pdf

    // h2 console
    http.csrf().disable();
    http.headers().frameOptions().disable();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(authenticationProvider);
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public PersistentTokenBasedRememberMeServices rememberMeAuthenticationProvider() {
    return new PersistentTokenBasedRememberMeServices(SECRET_KEY, userDetailsService, tokenRepository());
  }

  @Bean
  public PersistentTokenRepository tokenRepository() {
    JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
    tokenRepository.setCreateTableOnStartup(true);
    tokenRepository.setDataSource(dataSource);
    return tokenRepository;
  }

}
