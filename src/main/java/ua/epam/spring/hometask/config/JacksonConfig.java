package ua.epam.spring.hometask.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.util.List;

/**
 * Created by Ruslan_Kamashev on 7/13/2016.
 */
@Configuration
public class JacksonConfig {

    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper;
    }

    @Bean(name = "userJsonReader")
    public ObjectReader getUserJsonReader(ObjectMapper mapper) {
        return mapper.readerFor(new TypeReference<List<User>>() {
        });
    }

    @Bean(name = "eventJsonReader")
    public ObjectReader getEventJsonReader(ObjectMapper mapper) {
        return mapper.readerFor(new TypeReference<List<Event>>() {
        });
    }

}
