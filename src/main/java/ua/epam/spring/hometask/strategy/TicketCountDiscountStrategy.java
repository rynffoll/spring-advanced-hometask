package ua.epam.spring.hometask.strategy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Created by rynffoll on 27.03.16.
 */
@Component
public class TicketCountDiscountStrategy implements DiscountStrategy {

  @Value("${ticketCountDiscount.discount}")
  private byte discount;
  @Value("${ticketCountDiscount.ticketCount}")
  private long ticketCount;

  public byte getDiscount() {
    return discount;
  }

  public void setDiscount(byte discount) {
    this.discount = discount;
  }

  public long getTicketCount() {
    return ticketCount;
  }

  public void setTicketCount(long ticketCount) {
    this.ticketCount = ticketCount;
  }

  @Override
  public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
    byte currentDiscount = 0;
    long ticketsWithDiscount = 0;

    if (user != null) {
      long count = numberOfTickets + (user.getTickets().size() % ticketCount);
      ticketsWithDiscount = count / ticketCount;
    } else {
      ticketsWithDiscount = numberOfTickets / ticketCount;
    }

    currentDiscount = (byte) ((ticketsWithDiscount * discount) / numberOfTickets);

    return currentDiscount;
  }
}

