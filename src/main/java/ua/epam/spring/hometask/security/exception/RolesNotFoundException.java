package ua.epam.spring.hometask.security.exception;

/**
 * Created by Ruslan_Kamashev on 7/25/2016.
 */
public class RolesNotFoundException extends RuntimeException {
    public RolesNotFoundException() {
        super();
    }

    public RolesNotFoundException(String message) {
        super(message);
    }

    public RolesNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RolesNotFoundException(Throwable cause) {
        super(cause);
    }

    protected RolesNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
