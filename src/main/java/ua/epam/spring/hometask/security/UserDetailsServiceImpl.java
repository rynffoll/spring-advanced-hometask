package ua.epam.spring.hometask.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.UserService;
import ua.epam.spring.hometask.security.exception.RolesNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 24.07.16.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  private UserService userService;

  @Autowired
  public UserDetailsServiceImpl(UserService userService) {
    this.userService = userService;
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    User user = userService.getUserByEmail(email);
    if (user == null) {
      throw new UsernameNotFoundException("User " + email + " not found!");
    }

    List<String> roles = user.getRoles();
    if (roles == null || roles.isEmpty()) {
      throw new RolesNotFoundException("Roles not found! User is not authorized!");
    }

    List<SimpleGrantedAuthority> grantedAuthorities =
            roles.stream()
                    .map(r -> new SimpleGrantedAuthority(r))
                    .collect(Collectors.toList());

    return new org.springframework.security.core.userdetails.User(
            user.getEmail(),
            user.getPassword(),
            grantedAuthorities);
  }
}
