package ua.epam.spring.hometask.security;

/**
 * Created by rynffoll on 24.07.16.
 */
public class Roles {
  public static final String REGISTERED_USER = "REGISTERED_USER";
  public static final String BOOKING_MANAGER = "BOOKING_MANAGER";
}
