package ua.epam.spring.hometask.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by rynffoll on 24.07.16.
 */
@Service
public class UserDetailsAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

  private UserDetailsService userDetailsService;

  private PasswordEncoder passwordEncoder;

  @Autowired
  public UserDetailsAuthenticationProvider(UserDetailsService userDetailsService, PasswordEncoder passwordEncoder) {
    this.userDetailsService = userDetailsService;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  protected void additionalAuthenticationChecks(UserDetails userDetails,
                                                UsernamePasswordAuthenticationToken token)
          throws AuthenticationException {
    Object credentials = token.getCredentials();
    String password = userDetails.getPassword();

    if (credentials == null || password == null) {
      throw new BadCredentialsException("Credentials or password is null");
    }

    if (!passwordEncoder.matches(credentials.toString(), password)) {
      throw new BadCredentialsException("Invalid password");
    }

    logger.debug("Credentials is correct");
  }

  @Override
  protected UserDetails retrieveUser(String username,
                                     UsernamePasswordAuthenticationToken token)
          throws AuthenticationException {
    UserDetails userDetails = userDetailsService.loadUserByUsername(username);
    return userDetails;
  }
}
