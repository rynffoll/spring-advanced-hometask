package ua.epam.spring.hometask.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by rynffoll on 03.08.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class GetEventByIdRequest {
  private Long eventId;
}
