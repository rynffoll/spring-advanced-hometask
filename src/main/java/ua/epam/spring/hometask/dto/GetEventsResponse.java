package ua.epam.spring.hometask.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.epam.spring.hometask.domain.Event;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Created by rynffoll on 03.08.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class GetEventsResponse {
  private Collection<Event> events;
}
