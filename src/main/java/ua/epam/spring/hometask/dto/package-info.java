/**
 * Created by Ruslan_Kamashev on 8/4/2016.
 */
@XmlSchema(namespace = "http://localhost:8080/ws",
        elementFormDefault = XmlNsForm.QUALIFIED)
@XmlAccessorType(XmlAccessType.FIELD)
package ua.epam.spring.hometask.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;