package ua.epam.spring.hometask.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.epam.spring.hometask.domain.User;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Created by rynffoll on 04.08.16.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class GetUsersResponse {
  private Collection<User> users;
}
