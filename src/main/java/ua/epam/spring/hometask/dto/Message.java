package ua.epam.spring.hometask.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Ruslan_Kamashev on 8/3/2016.
 */
@Data
@AllArgsConstructor
public class Message {
  private String text;
}
