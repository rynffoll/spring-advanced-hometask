package ua.epam.spring.hometask.xml;

import ua.epam.spring.hometask.domain.Auditorium;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Ruslan_Kamashev on 8/3/2016.
 */
public class MapWithLocalDateTimeKeyXmlAdapter extends XmlAdapter<Auditoriums, NavigableMap<LocalDateTime, Auditorium>> {
  @Override
  public NavigableMap<LocalDateTime, Auditorium> unmarshal(Auditoriums v) throws Exception {
//    NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
//    v.getAuditoriums().forEach(a -> auditoriums.put(a.getKey(), a.getValue()));
//    return auditoriums;
    return null;
  }

  @Override
  public Auditoriums marshal(NavigableMap<LocalDateTime, Auditorium> v) throws Exception {
//    List<KeyValue<LocalDateTime, Auditorium>> auditoriums = v.entrySet().stream()
//            .map(e -> new KeyValue(e.getKey(), e.getValue()))
//            .collect(Collectors.toList());
//    return new Auditoriums(auditoriums);
    return null;
  }
}
