/**
 * Created by Ruslan_Kamashev on 8/3/2016.
 */
@XmlSchema(namespace = "http://localhost:8080/ws",
        elementFormDefault = XmlNsForm.QUALIFIED)
@XmlJavaTypeAdapters(value = {
        @XmlJavaTypeAdapter(value = LocalDateTimeXmlAdapter.class, type = LocalDateTime.class)
})
@XmlAccessorType(XmlAccessType.FIELD)
package ua.epam.spring.hometask.xml;

import com.migesok.jaxb.adapter.javatime.LocalDateTimeXmlAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.LocalDateTime;
