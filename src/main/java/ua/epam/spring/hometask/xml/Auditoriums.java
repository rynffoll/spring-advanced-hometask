package ua.epam.spring.hometask.xml;

import ua.epam.spring.hometask.domain.Auditorium;

import javax.xml.bind.annotation.XmlElementWrapper;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Ruslan_Kamashev on 8/3/2016.
 */
public class Auditoriums {

  private List<KeyValue> auditoriums;

  public Auditoriums() {
  }

  public Auditoriums(List<KeyValue> auditoriums) {
    this.auditoriums = auditoriums;
  }

  public List<KeyValue> getAuditoriums() {
    return auditoriums;
  }

  public void setAuditoriums(List<KeyValue> auditoriums) {
    this.auditoriums = auditoriums;
  }
}
