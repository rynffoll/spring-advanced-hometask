package ua.epam.spring.hometask.xml;

import java.time.LocalDateTime;

/**
 * Created by Ruslan_Kamashev on 8/3/2016.
 */
public class KeyValue {
  private LocalDateTime key;
  private Auditoriums value;

  public KeyValue() {
  }

  public KeyValue(LocalDateTime key, Auditoriums value) {
    this.key = key;
    this.value = value;
  }

  public LocalDateTime getKey() {
    return key;
  }

  public void setKey(LocalDateTime key) {
    this.key = key;
  }

  public Auditoriums getValue() {
    return value;
  }

  public void setValue(Auditoriums value) {
    this.value = value;
  }
}
