package ua.epam.spring.hometask.service.exception;

/**
 * Created by Ruslan_Kamashev on 7/29/2016.
 */
public class InsufficientFundsException extends RuntimeException {
  public InsufficientFundsException() {
    super("Not enough money on the account.");
  }
}
