package ua.epam.spring.hometask.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.dao.AuditoriumDAO;
import ua.epam.spring.hometask.dao.EventDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.service.EventService;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 27.03.16.
 */
@Service
public class EventServiceImpl implements EventService {

  @Autowired
  private EventDAO eventDAO;
  @Autowired
  private AuditoriumDAO auditoriumDAO;

  @Nullable
  @Override
  public Event getByName(@Nonnull String name) {
    return eventDAO.getByName(name);
  }

  @Override
  public Event save(@Nonnull Event event) {
    return eventDAO.save(event);
  }

  @Override
  public void save(List<Event> events) {
    eventDAO.save(events);
  }

  @Override
  public void remove(@Nonnull Event event) {
    eventDAO.remove(event);
  }

  @Override
  public Event getById(@Nonnull Long id) {
    return eventDAO.getById(id);
  }

  @Nonnull
  @Override
  public Collection<Event> getAll() {
    return eventDAO.getAll().stream().sorted((e1, e2) -> Long.compare(e1.getId(), e2.getId())).collect(Collectors.toList());
  }
}
