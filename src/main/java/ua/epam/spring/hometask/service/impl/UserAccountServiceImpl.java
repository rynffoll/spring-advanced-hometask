package ua.epam.spring.hometask.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.epam.spring.hometask.dao.UserAccountDAO;
import ua.epam.spring.hometask.domain.UserAccount;
import ua.epam.spring.hometask.service.UserAccountService;

import java.math.BigDecimal;

/**
 * Created by Ruslan_Kamashev on 7/29/2016.
 */
@Service
@Slf4j
public class UserAccountServiceImpl implements UserAccountService {

  private UserAccountDAO userAccountDAO;

  @Autowired
  public UserAccountServiceImpl(UserAccountDAO userAccountDAO) {
    this.userAccountDAO = userAccountDAO;
  }

  @Override
  public void refilling(UserAccount userAccount, BigDecimal amount) {
    BigDecimal prevAmount = userAccount.getAmount();
    if (prevAmount == null) {
      userAccount.setAmount(amount);
    } else {
      userAccount.setAmount(userAccount.getAmount().add(amount));
    }

    userAccountDAO.save(userAccount);

    log.debug("Refilling user account (id = {}, prev. amount = {}, cur. amount = {}) at {}",
            userAccount.getId(), prevAmount, userAccount.getAmount(),
            amount);
  }

  @Override
  public boolean check(UserAccount userAccount, BigDecimal amount) {
    UserAccount selectedUserAccount = userAccountDAO.getById(userAccount.getId());
    return selectedUserAccount.getAmount().compareTo(amount) >= 0;
  }

  @Override
  public void withdraw(UserAccount userAccount, BigDecimal amount) {
    BigDecimal prevAmount = userAccount.getAmount();
    userAccount.setAmount(userAccount.getAmount().subtract(amount));
    userAccountDAO.save(userAccount);

    log.debug("Withdraw {} from user account (id = {}, prev. amount = {}, cur. amount = {})",
            amount,
            userAccount.getId(), prevAmount, userAccount.getAmount());
  }
}
