package ua.epam.spring.hometask.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.epam.spring.hometask.dao.EventDAO;
import ua.epam.spring.hometask.dao.TicketDAO;
import ua.epam.spring.hometask.dao.UserDAO;
import ua.epam.spring.hometask.domain.*;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.DiscountService;
import ua.epam.spring.hometask.service.UserAccountService;
import ua.epam.spring.hometask.service.exception.InsufficientFundsException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 27.03.16.
 */
@Service
@Slf4j
public class BookingServiceImpl implements BookingService {

  @Value("${bookingService.markupForHighRatingEvent}")
  private double markupForHighRatingEvent;
  @Value("${bookingService.markupForVipSeat}")
  private double markupForVipSeat;

  private DiscountService discountService;
  private UserAccountService userAccountService;

  private TicketDAO ticketDAO;
  private EventDAO eventDAO;
  private UserDAO userDAO;

  @Autowired
  public BookingServiceImpl(DiscountService discountService, UserAccountService userAccountService, TicketDAO ticketDAO, EventDAO eventDAO, UserDAO userDAO) {
    this.discountService = discountService;
    this.userAccountService = userAccountService;
    this.ticketDAO = ticketDAO;
    this.eventDAO = eventDAO;
    this.userDAO = userDAO;
  }

  @Override
  public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime, @Nullable User user, @Nonnull Set<Long> seats) {
    double totalPrice = 0;

    long numberOfTickets = seats.size();
    byte discount = discountService.getDiscount(user, event, dateTime, numberOfTickets);

    double basePrice = event.getBasePrice();
    String rating = event.getRating();
    double currentPrice = (rating.equals(EventRating.HIGH)) ? basePrice * markupForHighRatingEvent : basePrice;

    Auditorium auditorium = event.getAuditoriums().entrySet()
            .stream()
            .filter(entry -> entry.getKey().equals(dateTime))
            .map(Map.Entry::getValue)
            .findFirst()
            .get();
    Set<Long> vipSeats = auditorium.getVipSeats();

    totalPrice = seats
            .stream()
            .mapToDouble(seat -> (vipSeats.contains(seat) ? basePrice * markupForVipSeat : currentPrice))
            .sum();

    totalPrice /= (discount > 0) ? ((double) discount / 100.0) : 1.0;
    return totalPrice;
  }

  @Transactional
  @Override
  public void bookTickets(@Nonnull Set<Ticket> tickets) {
    // all tickets have same event, date time and user
    Ticket firstTicket = tickets.stream().findFirst().get();
    User user = firstTicket.getUser();
    Event event = firstTicket.getEvent();
    LocalDateTime dateTime = firstTicket.getDateTime();
    Set<Long> seats = tickets.stream().map(t -> t.getSeat()).collect(Collectors.toSet());

    double totalPrice = getTicketsPrice(event, dateTime, user, seats);

    boolean bookingIsAvailable = userAccountService.check(user.getUserAccount(), BigDecimal.valueOf(totalPrice));
    if (!bookingIsAvailable) {
      throw new InsufficientFundsException();
    }

    userAccountService.withdraw(user.getUserAccount(), BigDecimal.valueOf(totalPrice));

    for (Ticket ticket : tickets) {
      ticket.setPurchased(true);
      ticketDAO.save(ticket);
      user.getTickets().add(ticket);
    }
  }

  @Nonnull
  @Override
  public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
    return ticketDAO.getAll()
            .stream()
            .filter(ticket -> ticket.isPurchased() && ticket.getEvent().equals(event) && ticket.getDateTime().equals(dateTime))
            .collect(Collectors.toSet());
  }
}
