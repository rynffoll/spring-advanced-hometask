package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.UserAccount;

import java.math.BigDecimal;

/**
 * Created by Ruslan_Kamashev on 7/29/2016.
 */
public interface UserAccountService {

  void refilling(UserAccount userAccount, BigDecimal amount);

  boolean check(UserAccount userAccount, BigDecimal amount);

  void withdraw(UserAccount userAccount, BigDecimal amount);

}
