package ua.epam.spring.hometask.aspect.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.aspect.dao.CounterDAO;
import ua.epam.spring.hometask.domain.Event;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rynffoll on 13.04.16.
 */
@Repository
public class CounterDAOImpl implements CounterDAO {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  @Override
  public int getCounterForNameRequest(Event event) {
    try {
      return jdbcTemplate.queryForObject(
              "SELECT counter FROM counterForNameRequest WHERE eventId = :eventId",
              new MapSqlParameterSource("eventId", event.getId()),
              Integer.class);
    } catch (EmptyResultDataAccessException e) {
      return 0;
    }
  }

  @Override
  public int getCounterForPriceRequest(Event event) {
    try {
      return jdbcTemplate.queryForObject(
              "SELECT counter FROM counterForPriceRequest WHERE eventId = :eventId",
              new MapSqlParameterSource("eventId", event.getId()),
              Integer.class);
    } catch (EmptyResultDataAccessException e) {
      return 0;
    }
  }

  @Override
  public int getCounterBookingCounter(Event event) {
    try {
      return jdbcTemplate.queryForObject(
              "SELECT counter FROM bookingCounter WHERE eventId = :eventId",
              new MapSqlParameterSource("eventId", event.getId()),
              Integer.class);
    } catch (EmptyResultDataAccessException e) {
      return 0;
    }
  }

  @Override
  public void updateForNameRequest(Event event, Integer counter) {
    Map<String, ?> params = new HashMap<String, Object>() {{
      put("eventId", event.getId());
      put("counter", counter);
    }};
    try {
      jdbcTemplate.queryForObject(
              "SELECT counter FROM counterForNameRequest WHERE eventId = :eventId",
              params,
              Long.class);
      jdbcTemplate.update(
              "UPDATE counterForNameRequest SET counter = :counter WHERE eventId = :eventId",
              params);
    } catch (EmptyResultDataAccessException e) {
      jdbcTemplate.update(
              "INSERT INTO counterForNameRequest (eventId, counter) VALUES(:eventId, :counter)",
              params);
    }
  }

  @Override
  public void updateForPriceRequest(Event event, Integer counter) {
    Map<String, ?> params = new HashMap<String, Object>() {{
      put("eventId", event.getId());
      put("counter", counter);
    }};
    try {
      jdbcTemplate.queryForObject(
              "SELECT counter FROM counterForPriceRequest WHERE eventId = :eventId",
              params,
              Long.class);
      jdbcTemplate.update(
              "UPDATE counterForPriceRequest SET counter = :counter WHERE eventId = :eventId",
              params);
    } catch (EmptyResultDataAccessException e) {
      jdbcTemplate.update(
              "INSERT INTO counterForPriceRequest (eventId, counter) VALUES(:eventId, :counter)",
              params);
    }
  }

  @Override
  public void updateBookingCounter(Event event, Integer counter) {
    Map<String, ?> params = new HashMap<String, Object>() {{
      put("eventId", event.getId());
      put("counter", counter);
    }};
    try {
      jdbcTemplate.queryForObject(
              "SELECT counter FROM bookingCounter WHERE eventId = :eventId",
              params,
              Long.class);
      jdbcTemplate.update(
              "UPDATE bookingCounter SET counter = :counter WHERE eventId = :eventId",
              params);
    } catch (EmptyResultDataAccessException e) {
      jdbcTemplate.update(
              "INSERT INTO bookingCounter (eventId, counter) VALUES(:eventId, :counter)",
              params);
    }
  }
}
