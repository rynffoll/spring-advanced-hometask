package ua.epam.spring.hometask.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.aspect.dao.LuckyEventDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by rynffoll on 03.04.16.
 */
@Aspect
@Component
public class LuckyWinnerAspect {

  @Autowired
  private LuckyEventDAO luckyEventDAO;

  private static final int LUCKY_NUMBER = 42;

  @Around("execution(double ua.epam.spring.hometask.service.impl.BookingServiceImpl.getTicketsPrice(..)))")
  public double callGetTicketsPrice(ProceedingJoinPoint joinPoint) throws Throwable {
    Object[] args = joinPoint.getArgs();
    Event event = (Event) args[0];
    User user = (User) args[2];

    if (isLucky()) {
      if (user != null) {
        luckyEventDAO.save(user, event);

        System.out.println("[ASPECT] Lucky user is " + user);
      }

      return 0.0;
    }

    return (double) joinPoint.proceed(args);
  }

  private boolean isLucky() {
    return (new Random().nextInt() % 100) == LUCKY_NUMBER;
  }

}
