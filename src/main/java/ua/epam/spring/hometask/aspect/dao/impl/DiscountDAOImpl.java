package ua.epam.spring.hometask.aspect.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.aspect.dao.DiscountDAO;
import ua.epam.spring.hometask.domain.User;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by rynffoll on 13.04.16.
 */
@Repository
public class DiscountDAOImpl implements DiscountDAO {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  @Override
  public int getCounterByDiscountStrategy(String className) {
    try {
      return jdbcTemplate.queryForObject(
              "SELECT counter FROM counterByDiscountStrategy WHERE className = :className",
              new MapSqlParameterSource("className", className),
              Integer.class);
    } catch (EmptyResultDataAccessException e) {
      return 0;
    }
  }

  @Override
  public int getCounterByUserAndDiscountStrategy(User user, String className) {
    Map<String, ?> params = new HashMap<String, Object>() {{
      put("userId", user.getId());
      put("className", className);
    }};
    try {
      return jdbcTemplate.queryForObject(
              "SELECT counter FROM counterByUserAndDiscountStrategy WHERE userId = :userId AND className = :className",
              params,
              Integer.class);
    } catch (EmptyResultDataAccessException e) {
      return 0;
    }
  }

  @Override
  public void updateCounterByDiscountStrategy(String className, Integer counter) {
    Map<String, ?> params = new HashMap<String, Object>() {{
      put("className", className);
      put("counter", counter);
    }};
    try {
      jdbcTemplate.queryForObject(
              "SELECT counter FROM counterByDiscountStrategy WHERE className = :className",
              params,
              Long.class);
      jdbcTemplate.update(
              "UPDATE counterByDiscountStrategy SET counter = :counter WHERE className = :className",
              params);
    } catch (EmptyResultDataAccessException e) {
      jdbcTemplate.update(
              "INSERT INTO counterByDiscountStrategy (className, counter) VALUES(:className, :counter)",
              params);
    }
  }

  @Override
  public void updateCounterByUserAndDiscountStrategy(User user, String className, Integer counter) {
    Map<String, ?> params = new HashMap<String, Object>() {{
      put("userId", user.getId());
      put("className", className);
      put("counter", counter);
    }};
    try {
      jdbcTemplate.queryForObject(
              "SELECT counter FROM counterByUserAndDiscountStrategy WHERE className = :className AND userId = :userId",
              params,
              Long.class);
      jdbcTemplate.update(
              "UPDATE counterByUserAndDiscountStrategy SET counter = :counter WHERE className = :className AND userId = :userId",
              params);
    } catch (EmptyResultDataAccessException e) {
      jdbcTemplate.update(
              "INSERT INTO counterByUserAndDiscountStrategy (userId, className, counter) VALUES(:userId, :className, :counter)",
              params);
    }
  }
}
