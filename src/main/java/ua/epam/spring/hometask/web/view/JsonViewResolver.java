package ua.epam.spring.hometask.web.view;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.Locale;

/**
 * Created by rynffoll on 03.08.16.
 */
@Component
public class JsonViewResolver implements ViewResolver {
  @Override
  public View resolveViewName(String s, Locale locale) throws Exception {
    MappingJackson2JsonView view = new MappingJackson2JsonView();
    view.setPrettyPrint(true);
    return view;
  }
}
