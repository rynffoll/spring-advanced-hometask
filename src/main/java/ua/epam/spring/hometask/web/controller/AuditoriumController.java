package ua.epam.spring.hometask.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.service.AuditoriumService;

import java.util.Set;

@Controller
public class AuditoriumController {

  private final AuditoriumService auditoriumService;

  @Autowired
  public AuditoriumController(AuditoriumService auditoriumService) {
    this.auditoriumService = auditoriumService;
  }

  @RequestMapping("/auditoriums")
  public String showAuditoriums(Model model) {
    Set<Auditorium> auditoriums = auditoriumService.getAll();
    model.addAttribute("auditoriums", auditoriums);
    return "auditoriums";
  }
}
