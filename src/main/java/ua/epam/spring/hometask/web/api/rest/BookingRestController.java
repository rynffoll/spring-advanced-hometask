package ua.epam.spring.hometask.web.api.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.util.EncodingUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ua.epam.spring.hometask.dao.TicketDAO;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.EventService;
import ua.epam.spring.hometask.service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Ruslan_Kamashev on 8/2/2016.
 */
@RestController("/api/booking")
@Slf4j
public class BookingRestController {

  private final BookingService bookingService;
  private final EventService eventService;
  private final UserService userService;
  private final TicketDAO ticketDAO;

  @Autowired
  public BookingRestController(BookingService bookingService, EventService eventService, UserService userService, TicketDAO ticketDAO) {
    this.bookingService = bookingService;
    this.eventService = eventService;
    this.userService = userService;
    this.ticketDAO = ticketDAO;
  }

  @GetMapping(produces = MediaType.APPLICATION_PDF_VALUE)
  public Set<Ticket> getPurchasedTicketsForPdfType(@RequestParam("eventId") Long eventId,
                                                   @RequestParam("dateTime") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime dateTime,
                                                   HttpServletResponse response) {
    response.setContentType(MediaType.APPLICATION_PDF_VALUE); // set content type for response
    return getPurchasedTickets(eventId, dateTime);
  }

  @GetMapping
  public Set<Ticket> getPurchasedTicketsForOtherTypes(@RequestParam("eventId") Long eventId,
                                                      @RequestParam("dateTime") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime dateTime) {
    return getPurchasedTickets(eventId, dateTime);
  }

  private Set<Ticket> getPurchasedTickets(Long eventId,
                                          LocalDateTime dateTime) {
    Event event = eventService.getById(eventId);
    Set<Ticket> tickets = bookingService.getPurchasedTicketsForEvent(event, dateTime);
    return tickets;
  }

  @PostMapping(produces = MediaType.APPLICATION_PDF_VALUE)
  public Set<Ticket> bookTicketForPdfType(@RequestParam("userId") Long userId,
                                          @RequestParam("eventId") Long eventId,
                                          @RequestParam("dateTime") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime dateTime,
                                          @RequestParam("seats") Set<Long> seats,
                                          HttpServletResponse response) {
    response.setContentType(MediaType.APPLICATION_PDF_VALUE); // set content type for response
    return bookTickets(userId, eventId, dateTime, seats);
  }

  @PostMapping
  public Set<Ticket> bookTicketForOtherTypes(@RequestParam("userId") Long userId,
                                             @RequestParam("eventId") Long eventId,
                                             @RequestParam("dateTime") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime dateTime,
                                             @RequestParam("seats") Set<Long> seats) {
    return bookTickets(userId, eventId, dateTime, seats);
  }

  private Set<Ticket> bookTickets(Long userId,
                                  Long eventId,
                                  LocalDateTime dateTime,
                                  Set<Long> seats) {
    User user = userService.getById(userId);
    Event event = eventService.getById(eventId);
    Set<Ticket> tickets = seats.stream()
            .map(s -> new Ticket(user, event, dateTime, s))
            .collect(Collectors.toSet());
    bookingService.bookTickets(tickets);
    return tickets.stream()
            .map(t -> {
              t.setPurchased(true);
              return t;
            })
            .collect(Collectors.toSet());
  }
}
