package ua.epam.spring.hometask.web.view;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.document.AbstractPdfView;
import ua.epam.spring.hometask.domain.Ticket;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ruslan_Kamashev on 7/14/2016.
 */
public class PdfView extends AbstractPdfView {
  @Override
  protected void buildPdfDocument(Map<String, Object> model,
                                  Document document,
                                  PdfWriter writer,
                                  HttpServletRequest request, HttpServletResponse response) throws Exception {
    @SuppressWarnings("unchecked")
    Set<Ticket> tickets = (Set<Ticket>) model.get("tickets");

    Paragraph paragraph = new Paragraph("Purchased tickets");
    PdfPTable table = new PdfPTable(3);

    // table settings
    table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
    table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
    Font headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

    // header
    table.addCell(new Phrase("Name", headerFont));
    table.addCell(new Phrase("Date and time", headerFont));
    table.addCell(new Phrase("Seat", headerFont));

    // content
    for (Ticket ticket : tickets) {
      table.addCell(ticket.getEvent().getName());
      table.addCell(ticket.getDateTime().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)));
      table.addCell(Long.toString(ticket.getSeat()));
    }

    paragraph.add(table);
    document.add(paragraph);
  }

}
