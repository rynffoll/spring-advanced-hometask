package ua.epam.spring.hometask.web.view;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import java.util.Locale;

/**
 * Created by rynffoll on 03.08.16.
 */
@Component
public class PdfViewResolver implements ViewResolver {
  @Override
  public View resolveViewName(String s, Locale locale) throws Exception {
    PdfView view = new PdfView();
    return view;
  }
}
