package ua.epam.spring.hometask.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Ruslan_Kamashev on 7/13/2016.
 */
@ControllerAdvice("ua.epam.spring.hometask.web.controller")
@Slf4j
public class ExceptionHandlerController {

  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(RuntimeException.class)
  public String handleException(Model model, Exception e) {
    log.error("Error", e);
    model.addAttribute("errorMessage", e.getMessage());
    return "error";
  }

}
