package ua.epam.spring.hometask.web.api.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ua.epam.spring.hometask.dto.Message;

/**
 * Created by Ruslan_Kamashev on 8/3/2016.
 */
@RestControllerAdvice("ua.epam.spring.hometask.web.controller")
@Slf4j
public class ExceptionHandlerRestController {

  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(RuntimeException.class)
  public Message handleException(Exception e) {
    log.error("Error", e);
    return new Message(e.getMessage());
  }

}
