package ua.epam.spring.hometask.web.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.EventService;
import ua.epam.spring.hometask.service.UserService;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Ruslan_Kamashev on 7/13/2016.
 */
@Controller
@RequestMapping("/booking")
@Slf4j
public class BookingController {

  private final BookingService bookingService;
  private final EventService eventService;
  private final UserService userService;

  @Autowired
  public BookingController(BookingService bookingService, EventService eventService, UserService userService) {
    this.bookingService = bookingService;
    this.eventService = eventService;
    this.userService = userService;
  }

  @RequestMapping
  public String showPage(Model model) {
    Collection<Event> events = eventService.getAll();
    model.addAttribute("events", events);
    return "booking";
  }

  @RequestMapping("/book-tickets")
  public String getTicketsPrice(Model model) {
    Collection<Event> events = eventService.getAll();
    model.addAttribute("events", events);

    return "book-tickets";
  }

  @RequestMapping("/book-tickets/book")
  public String bookTicket(Model model,
                           Principal principal, // user
                           @RequestParam("eventId") Long eventId,
                           @RequestParam(name = "airDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime airDate,
                           @RequestParam(name = "seats", required = false) String seats) {
    String email = principal.getName();
    User user = userService.getUserByEmail(email);

    Event selectedEvent = null;
    if (eventId != null) {
      selectedEvent = eventService.getById(eventId);
      model.addAttribute("selectedEvent", selectedEvent);
    }

    if (airDate != null) {
      model.addAttribute("selectedAirDate", airDate);
    }

    if (seats != null && !seats.trim().isEmpty()) {
      List<Long> listOfSeats = Arrays.stream(seats.split(","))
              .map(s -> Long.parseLong(s.trim()))
              .collect(Collectors.toList());
      Set<Ticket> tickets = new HashSet<>();
      for (Long seat : listOfSeats) {
        tickets.add(new Ticket(user, selectedEvent, airDate, seat));
      }
      bookingService.bookTickets(tickets);
      log.info("Book {} tickets on {}:{} by {}:{} at {}",
              tickets.size(),
              selectedEvent.getId(), selectedEvent.getName(),
              user.getId(), user.getFirstName() + " " + user.getLastName(),
              listOfSeats);
    }

    return getTicketsPrice(model);
  }

  @RequestMapping(path = "/tickets", produces = MediaType.APPLICATION_PDF_VALUE)
  public ModelAndView showTickets(ModelAndView modelAndView,
                                  @RequestParam("eventId") long eventId,
                                  @RequestParam("dateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateTime) {
    Event event = eventService.getById(eventId);
    Set<Ticket> tickets = bookingService.getPurchasedTicketsForEvent(event, dateTime);

    log.info("Purchased {} tickets on {}:{} at {}",
            tickets.size(), event.getId(), event.getName(), dateTime);

    modelAndView.addObject("tickets", tickets);
    modelAndView.setViewName("pdf");

    return modelAndView;
  }
}
