package ua.epam.spring.hometask.web.api.soap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.dto.*;
import ua.epam.spring.hometask.service.UserService;

import java.util.Collection;

/**
 * Created by Ruslan_Kamashev on 8/2/2016.
 */
@Endpoint
public class UserEndpoint {

  private static final String NAMESPACE_URI = "http://localhost:8080/ws";

  private UserService userService;

  @Autowired
  public UserEndpoint(UserService userService) {
    this.userService = userService;
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUsersRequest")
  @ResponsePayload
  public GetUsersResponse getUsers(@RequestPayload GetUsersRequest usersRequest) {
    Collection<User> users = userService.getAll();
    return new GetUsersResponse(users);
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserByIdRequest")
  @ResponsePayload
  public GetUserByIdResponse getUserById(@RequestPayload GetUserByIdRequest userByIdRequest) {
    User user = userService.getById(userByIdRequest.getUserId());
    return new GetUserByIdResponse(user);
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getUserByEmailRequest")
  @ResponsePayload
  public GetUserByEmailResponse getUserByEmail(@RequestPayload GetUserByEmailRequest userByEmailRequest) {
    User user = userService.getUserByEmail(userByEmailRequest.getEmail());
    return new GetUserByEmailResponse(user);
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveUserRequest")
  @ResponsePayload
  public SaveUserResponse saveUser(@RequestPayload SaveUserRequest userRequest) {
    User user = userService.save(userRequest.getUser());
    return new SaveUserResponse(user);
  }

  @PayloadRoot(namespace = NAMESPACE_URI, localPart = "removeUserRequest")
  @ResponsePayload
  public RemoveUserResponse removeUser(@RequestPayload RemoveUserRequest userRequest) {
    User userForRemove = userService.getById(userRequest.getUserId());
    userService.remove(userForRemove);
    return new RemoveUserResponse("User removed.");
  }
}
