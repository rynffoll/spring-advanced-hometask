package ua.epam.spring.hometask.web.converter;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;
import ua.epam.spring.hometask.domain.Ticket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.List;

/**
 * Created by rynffoll on 03.08.16.
 */
@Component
public class PdfMessageConverter implements HttpMessageConverter<Collection<Ticket>> {
  @Override
  public boolean canRead(Class<?> aClass, MediaType mediaType) {
    return false;
  }

  @Override
  public boolean canWrite(Class<?> aClass, MediaType mediaType) {
    boolean canWrite = getSupportedMediaTypes().contains(mediaType)
            && aClass.isAssignableFrom(new HashSet<Ticket>().getClass());
    return canWrite;
  }

  @Override
  public List<MediaType> getSupportedMediaTypes() {
    return Collections.singletonList(MediaType.APPLICATION_PDF);
  }

  @Override
  public Collection<Ticket> read(Class<? extends Collection<Ticket>> aClass, HttpInputMessage httpInputMessage) throws IOException, HttpMessageNotReadableException {
    return null;
  }

  @Override
  public void write(Collection<Ticket> tickets, MediaType contentType, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
    try (OutputStream outputStream = outputMessage.getBody()) {
      Document document = new Document();

      PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream);
      document.open();

      Paragraph paragraph = new Paragraph("Purchased tickets");
      PdfPTable table = new PdfPTable(3);

      // table settings
      table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
      table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
      Font headerFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

      // header
      table.addCell(new Phrase("Name", headerFont));
      table.addCell(new Phrase("Date and time", headerFont));
      table.addCell(new Phrase("Seat", headerFont));

      // content
      for (Ticket ticket : tickets) {
        table.addCell(ticket.getEvent().getName());
        table.addCell(ticket.getDateTime().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)));
        table.addCell(Long.toString(ticket.getSeat()));
      }

      paragraph.add(table);

      document.add(paragraph);
      document.close();
    } catch (DocumentException e) {
      throw new IOException("Creating document error", e);
    }
  }
}
