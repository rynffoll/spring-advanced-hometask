package ua.epam.spring.hometask.web.controller;

import com.fasterxml.jackson.databind.ObjectReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.service.EventService;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ruslan_Kamashev on 7/13/2016.
 */
@Controller
@RequestMapping("/events")
@Slf4j
public class EventController {

    private static final String EVENTS_TEMPLATE = "events";

    private final EventService eventService;
    private final ObjectReader jsonReader;

    @Autowired
    public EventController(EventService eventService, @Qualifier("eventJsonReader") ObjectReader jsonReader) {
        this.eventService = eventService;
        this.jsonReader = jsonReader;
    }

    @RequestMapping
    public String showEvents(Model model) {
        Collection<Event> events = eventService.getAll();
        model.addAttribute("events", events);
        return EVENTS_TEMPLATE;
    }

    @RequestMapping(params = "name")
    public String getEventByName(Model model, @RequestParam("name") String name) {
        Event event = eventService.getByName(name);
        model.addAttribute("event", event);
        model.addAttribute("name", name);
        return EVENTS_TEMPLATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String loadEventsFromFile(Model model, @RequestParam("file")MultipartFile file) throws IOException {
        List<Event> events = jsonReader.readValue(file.getInputStream());

        log.info("Load {} events from file", events.size());

        eventService.save(events);
        return showEvents(model);
    }
}
