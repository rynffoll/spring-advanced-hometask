package ua.epam.spring.hometask.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

/**
 * Created by Ruslan_Kamashev on 7/13/2016.
 */
@Controller
public class MainController {

  @RequestMapping("/")
  public String showHomePage(Model model, Principal principal) {
    if (principal != null) {
      String name = principal.getName();
      model.addAttribute("username", name);
    }
    return "home";
  }
}
