package ua.epam.spring.hometask.web.controller;

import com.fasterxml.jackson.databind.ObjectReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.UserService;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ruslan_Kamashev on 7/13/2016.
 */
@Controller
@RequestMapping("/users")
@Slf4j
public class UserController {

    private static final String USER_TEMPLATE = "users";
    private final UserService userService;

    private final ObjectReader jsonReader;

    @Autowired
    public UserController(UserService userService, @Qualifier("userJsonReader") ObjectReader jsonReader) {
        this.userService = userService;
        this.jsonReader = jsonReader;
    }

    @RequestMapping
    public String showUsers(Model model) {
        Collection<User> users = userService.getAll();
        model.addAttribute("users", users);
        return USER_TEMPLATE;
    }

    @RequestMapping(params = "email")
    public String getUserByEmail(Model model, @RequestParam("email") String email) {
        User user = userService.getUserByEmail(email);
        model.addAttribute("user", user);
        model.addAttribute("email", email);
        return USER_TEMPLATE;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String loadUsersFromFile(Model model, @RequestParam("file") MultipartFile file) throws IOException {
        List<User> users = jsonReader.readValue(file.getInputStream());

        log.info("Load {} users from file", users.size());

        userService.save(users);
        return showUsers(model);
    }
}
