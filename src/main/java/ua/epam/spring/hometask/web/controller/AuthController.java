package ua.epam.spring.hometask.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by rynffoll on 24.07.16.
 */
@Controller
public class AuthController {

  @GetMapping("/login")
  public String showLoginPage() {
    return "login";
  }
}
