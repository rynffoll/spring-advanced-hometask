package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.Event;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by rynffoll on 27.03.16.
 */
public interface EventDAO extends AbstractDomainObjectDAO<Event> {

  @Nullable Event getByName(@Nonnull String name);

  void save(List<Event> events);

}
