package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.Auditorium;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

/**
 * Created by rynffoll on 27.03.16.
 */
public interface AuditoriumDAO {

  @Nullable Auditorium getByName(@Nonnull String name);

  @Nonnull Set<Auditorium> getAll();
}
