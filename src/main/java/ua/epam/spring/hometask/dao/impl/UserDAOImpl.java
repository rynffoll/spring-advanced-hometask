package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.dao.UserAccountDAO;
import ua.epam.spring.hometask.dao.UserDAO;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.domain.UserAccount;
import ua.epam.spring.hometask.util.DateConverter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by rynffoll on 27.03.16.
 */
@Repository
public class UserDAOImpl implements UserDAO {

  private static final String SELECT_BY_EMAIL = "SELECT * FROM user WHERE email = :email ORDER BY id";
  private static final String SELECT_BY_ID = "SELECT * FROM user WHERE id = :id ORDER BY id";
  private static final String INSERT = "INSERT INTO user (firstName, lastName, email, birthday, roles, password) VALUES(:firstName, :lastName, :email, :birthday, :roles, :password)";
  private static final String UPDATE = "UPDATE user SET firstName= :firstName, lastName = :lastName, email = :email, birthday = :birthday, roles = :roles, password = :password WHERE id = :id";
  private static final String DELETE_BY_ID = "DELETE FROM user WHERE id = :id";
  private static final String SELECT_ALL = "SELECT * FROM user ORDER BY id";

  private NamedParameterJdbcTemplate jdbcTemplate;
  private UserAccountDAO userAccountDAO;

  @Autowired
  public UserDAOImpl(NamedParameterJdbcTemplate jdbcTemplate, UserAccountDAO userAccountDAO) {
    this.jdbcTemplate = jdbcTemplate;
    this.userAccountDAO = userAccountDAO;
  }

  @Nullable
  @Override
  public User getUserByEmail(@Nonnull String email) {
    try {
      SqlParameterSource params = new MapSqlParameterSource("email", email);
      User user = jdbcTemplate.queryForObject(SELECT_BY_EMAIL, params, this::mapUser);
      user.setUserAccount(userAccountDAO.getByUser(user)); // add account
      return user;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public User save(@Nonnull User user) {
    if (user.getId() == null) {
      KeyHolder keyHolder = new GeneratedKeyHolder();
      MapSqlParameterSource sqlParams = mapUserToParams(user);

      jdbcTemplate.update(INSERT, sqlParams, keyHolder);
      user.setId(keyHolder.getKey().longValue());
    } else {
      jdbcTemplate.update(UPDATE, mapUserToParams(user).addValue("id", user.getId()));
    }
    userAccountDAO.save(user.getUserAccount()); // save account

    return user;
  }

  @Override
  public void save(@Nonnull List<User> users) {
    List<SqlParameterSource> sqlParams = new ArrayList<>(users.size());
    for (User user : users) {
      MapSqlParameterSource param = mapUserToParams(user);
      sqlParams.add(param);
    }

    SqlParameterSource[] sqlParamsArray = new SqlParameterSource[sqlParams.size()];
    jdbcTemplate.batchUpdate(INSERT, sqlParams.toArray(sqlParamsArray));

    List<UserAccount> userAccounts = users.stream().map(u -> u.getUserAccount()).collect(Collectors.toList());
    userAccountDAO.save(userAccounts);
  }

  @Override
  public void remove(@Nonnull User user) {
    jdbcTemplate.update(DELETE_BY_ID, new MapSqlParameterSource("id", user.getId()));
    if (user.getUserAccount() != null) {
      userAccountDAO.remove(user.getUserAccount()); // remove account
    }
  }

  @Override
  public User getById(@Nonnull Long id) {
    try {
      User user = jdbcTemplate.queryForObject(SELECT_BY_ID, new MapSqlParameterSource("id", id), this::mapUser);
      user.setUserAccount(userAccountDAO.getByUser(user));
      return user;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public Set<User> getAll() {
    try {
      Set<User> users = jdbcTemplate.query(SELECT_ALL, this::mapUser).stream().collect(Collectors.toSet());
      users.forEach(u -> u.setUserAccount(userAccountDAO.getByUser(u))); // set account
      return users;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  private User mapUser(ResultSet resultSet, int rowNumber) throws SQLException {
    User user = new User();
    user.setId(resultSet.getLong("id"));
    user.setFirstName(resultSet.getString("firstName"));
    user.setLastName(resultSet.getString("lastName"));
    user.setEmail(resultSet.getString("email"));
    Date birthday = resultSet.getDate("birthday");
    if (birthday != null) {
      user.setBirthday(birthday.toLocalDate());
    }
    String roles = resultSet.getString("roles");
    if (roles != null) {
      user.setRoles(roles);
    }
    String password = resultSet.getString("password");
    if (password != null) {
      user.setPassword(password);
    }
    return user;
  }

  private MapSqlParameterSource mapUserToParams(User user) {
    MapSqlParameterSource param = new MapSqlParameterSource()
            .addValue("firstName", user.getFirstName())
            .addValue("lastName", user.getLastName())
            .addValue("email", user.getEmail())
            .addValue("birthday", DateConverter.toDate(user.getBirthday()))
            .addValue("roles", user.getRolesAsString())
            .addValue("password", user.getPassword());
    return param;
  }
}
