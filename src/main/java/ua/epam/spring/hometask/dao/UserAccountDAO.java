package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.domain.UserAccount;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * Created by Ruslan_Kamashev on 7/29/2016.
 */
public interface UserAccountDAO extends AbstractDomainObjectDAO<UserAccount> {

  UserAccount getByUser(@Nonnull User user);

  void save(List<UserAccount> userAccounts);
}
