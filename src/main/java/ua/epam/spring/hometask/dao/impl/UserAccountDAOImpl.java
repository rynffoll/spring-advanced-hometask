package ua.epam.spring.hometask.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ua.epam.spring.hometask.dao.UserAccountDAO;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.domain.UserAccount;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Ruslan_Kamashev on 7/29/2016.
 */
@Repository
public class UserAccountDAOImpl implements UserAccountDAO {

  private static final String SELECT_ALL = "SELECT * FROM user_account";
  private static final String SELECT_BY_ID = "SELECT * FROM user_account WHERE id = :id";
  private static final String SELECT_BY_USER_ID = "SELECT * FROM user_account WHERE userId = :userId";
  private static final String DELETE_BY_ID = "DELETE FROM user_account WHERE id = :id";
  private static final String INSERT = "INSERT INTO user_account (amount) VALUES (:amount)";
  private static final String UPDATE = "UPDATE user_account SET amount = :amount WHERE id = :id";

  private NamedParameterJdbcTemplate jdbcTemplate;

  @Autowired
  public UserAccountDAOImpl(NamedParameterJdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }


  @Override
  public UserAccount save(@Nonnull UserAccount userAccount) {
    if (userAccount.getId() == null) {
      KeyHolder keyHolder = new GeneratedKeyHolder();
      MapSqlParameterSource sqlParams = mapUserAccountToParams(userAccount);

      jdbcTemplate.update(INSERT, sqlParams, keyHolder);
      userAccount.setId(keyHolder.getKey().longValue());
    } else {
      jdbcTemplate.update(UPDATE, mapUserAccountToParams(userAccount));
    }

    return userAccount;
  }

  @Override
  public void remove(@Nonnull UserAccount userAccount) {
    jdbcTemplate.update(DELETE_BY_ID, mapUserAccountToParams(userAccount));
  }

  @Override
  public UserAccount getById(@Nonnull Long id) {
    try {
      UserAccount userAccount = jdbcTemplate.queryForObject(SELECT_BY_ID, new MapSqlParameterSource("id", id), this::mapUserAccount);
      return userAccount;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public Collection<UserAccount> getAll() {
    try {
      List<UserAccount> userAccounts = jdbcTemplate.query(SELECT_ALL, this::mapUserAccount);
      return userAccounts;
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public UserAccount getByUser(@Nonnull User user) {
    try {
      return jdbcTemplate.queryForObject(SELECT_BY_USER_ID, new MapSqlParameterSource("userId", user.getId()), this::mapUserAccount);
    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }

  @Override
  public void save(List<UserAccount> userAccounts) {
    List<SqlParameterSource> sqlParams = new ArrayList<>(userAccounts.size());
    for (UserAccount account : userAccounts) {
      if (account == null) continue;
      MapSqlParameterSource param = mapUserAccountToParams(account);
      sqlParams.add(param);
    }

    SqlParameterSource[] sqlParamsArray = new SqlParameterSource[sqlParams.size()];
    jdbcTemplate.batchUpdate(INSERT, sqlParams.toArray(sqlParamsArray));
  }

  private UserAccount mapUserAccount(ResultSet rs, int rowNumber) throws SQLException {
    long id = rs.getLong("id");
    BigDecimal amount = rs.getBigDecimal("amount");

    UserAccount userAccount = new UserAccount();
    userAccount.setId(id);
    userAccount.setAmount(amount);

    return userAccount;
  }

  private MapSqlParameterSource mapUserAccountToParams(UserAccount userAccount) {
    return new MapSqlParameterSource()
            .addValue("id", userAccount.getId())
            .addValue("amount", userAccount.getAmount());
  }

}
