package ua.epam.spring.hometask.web.api.soap;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.client.core.WebServiceTemplate;
import ua.epam.spring.hometask.domain.Auditorium;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.dto.*;
import ua.epam.spring.hometask.service.EventService;

import java.time.LocalDateTime;
import java.util.TreeMap;
import java.util.TreeSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by rynffoll on 07.08.16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class EventEndpointTest {

  private static final String WS_URI = "http://localhost:8080/ws";

  private static WebServiceTemplate webServiceTemplate;
  @Autowired
  private EventService eventService;

  @BeforeClass
  public static void init() throws Exception {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    marshaller.setPackagesToScan("ua.epam.spring.hometask");
    marshaller.afterPropertiesSet();
    webServiceTemplate = new WebServiceTemplate();
    webServiceTemplate.setDefaultUri(WS_URI);
    webServiceTemplate.setMarshaller(marshaller);
    webServiceTemplate.setUnmarshaller(marshaller);
  }

  @Test
  public void getEvents() {
    GetEventsRequest request = new GetEventsRequest();
    GetEventsResponse response = (GetEventsResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getEvents(), is(eventService.getAll()));
  }

  @Test
  public void getEventById() {
    Long id = 1L;
    GetEventByIdRequest request = new GetEventByIdRequest(id);
    GetEventByIdResponse response = (GetEventByIdResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getEvent(), is(eventService.getById(id)));
  }

  @Test
  public void getEventByName() {
    String name = "Grand concert";
    GetEventByNameRequest request = new GetEventByNameRequest(name);
    GetEventByNameResponse response = (GetEventByNameResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getEvent(), is(eventService.getByName(name)));
  }

  @Test
  public void saveEvent() {
    SaveEventRequest request = new SaveEventRequest(generateEvent());
    SaveEventResponse response = (SaveEventResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getEvent(), notNullValue());
    assertThat(response.getEvent().getId(), notNullValue());
    assertThat(response.getEvent(), is(eventService.getById(response.getEvent().getId())));
  }

  @Test
  public void removeEvent() {
    /* Add test event */
    SaveEventRequest saveRequest = new SaveEventRequest(generateEvent());
    SaveEventResponse saveResponse = (SaveEventResponse) webServiceTemplate.marshalSendAndReceive(saveRequest);

    assertThat(saveResponse.getEvent(), notNullValue());
    assertThat(saveResponse.getEvent().getId(), notNullValue());
    assertThat(saveResponse.getEvent(), is(eventService.getById(saveResponse.getEvent().getId())));

    /* Remove test event */
    Long id = saveResponse.getEvent().getId();

    RemoveEventRequest request = new RemoveEventRequest(id);
    RemoveEventResponse response = (RemoveEventResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getMessage(), is("Event removed."));
    assertThat(eventService.getById(id), nullValue());
  }

  private Event generateEvent() {
    Event event = new Event() {
      {
        setName("Test Event");
        setRating("MID");
        setBasePrice(120.0);
        setAirDates(new TreeSet<LocalDateTime>() {{
          add(LocalDateTime.now());
        }});
        setAuditoriums(new TreeMap<LocalDateTime, Auditorium>());
      }
    };
    return event;
  }
}
