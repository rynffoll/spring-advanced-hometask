package ua.epam.spring.hometask.web.api.rest;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ua.epam.spring.hometask.domain.Ticket;

import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

/**
 * Created by rynffoll on 07.08.16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookingRestControllerTest {

  private static final String REST_URI = "http://localhost:8080/api/booking";

  private static RestTemplate restTemplate;

  @BeforeClass
  public static void init() {
    restTemplate = new RestTemplate();
    restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
  }

  @Test
  public void bookTicket() {
    Set<Ticket> tickets = restTemplate.postForObject(REST_URI, getPostParams(), new HashSet<Ticket>().getClass());

    assertThat(tickets.size(), is(1));
  }

  @Test
  public void bookTicketWithAcceptHeaderApplicationPdf() {
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_PDF));
    HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(getPostParams(), headers);

    ResponseEntity<String> response = restTemplate.postForEntity(REST_URI, entity, String.class);

    assertTrue(response.getHeaders().getContentType().includes(MediaType.APPLICATION_PDF));
  }

  @Test
  public void getPurchasedTicket() {
    Set<Ticket> tickets = restTemplate.getForObject(getUriWithParamsForGetRequest(), new HashSet<Ticket>().getClass());

    assertThat(tickets.size(), is(1));
  }

  @Test
  public void getPurchasedTicketWithAcceptHeaderApplicationPdf() {
    HttpHeaders headers = new HttpHeaders();
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_PDF));
    HttpEntity entity = new HttpEntity(headers);

    ResponseEntity<String> resposne = restTemplate.exchange(getUriWithParamsForGetRequest(), HttpMethod.GET, entity, String.class);

    assertTrue(resposne.getHeaders().getContentType().includes(MediaType.APPLICATION_PDF));
  }

  private MultiValueMap getPostParams() {
    MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>() {{
      put("userId", Collections.singletonList("2"));
      put("eventId", Collections.singletonList("1"));
      put("dateTime", Collections.singletonList("2016-08-01 12:00"));
      put("seats", Collections.singletonList("1"));
    }};
    return params;
  }

  private URI getUriWithParamsForGetRequest() {
    URI targetUrl = UriComponentsBuilder.fromHttpUrl(REST_URI)
            .queryParam("eventId", 1)
            .queryParam("dateTime", "2016-08-01 12:00")
            .build()
            .toUri();
    return targetUrl;
  }
}
