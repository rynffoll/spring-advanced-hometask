package ua.epam.spring.hometask.web.api.soap;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ws.client.core.WebServiceTemplate;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.domain.UserAccount;
import ua.epam.spring.hometask.dto.*;
import ua.epam.spring.hometask.service.UserService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by rynffoll on 07.08.16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserEndpointTest {

  private static final String WS_URI = "http://localhost:8080/ws";

  private static WebServiceTemplate webServiceTemplate;
  @Autowired
  private UserService UserService;

  @BeforeClass
  public static void init() throws Exception {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    marshaller.setPackagesToScan("ua.epam.spring.hometask");
    marshaller.afterPropertiesSet();
    webServiceTemplate = new WebServiceTemplate();
    webServiceTemplate.setDefaultUri(WS_URI);
    webServiceTemplate.setMarshaller(marshaller);
    webServiceTemplate.setUnmarshaller(marshaller);
  }

  @Test
  public void getUsers() {
    GetUsersRequest request = new GetUsersRequest();
    GetUsersResponse response = (GetUsersResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getUsers(), is(UserService.getAll()));
  }

  @Test
  public void getUserById() {
    Long id = 1L;
    GetUserByIdRequest request = new GetUserByIdRequest(id);
    GetUserByIdResponse response = (GetUserByIdResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getUser(), is(UserService.getById(id)));
  }

  @Test
  public void getUserByEmail() {
    String email = "bob@srv.com";
    GetUserByEmailRequest request = new GetUserByEmailRequest(email);
    GetUserByEmailResponse response = (GetUserByEmailResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getUser(), is(UserService.getUserByEmail(email)));
  }

  @Test
  public void saveUser() {
    SaveUserRequest request = new SaveUserRequest(generateUser());
    SaveUserResponse response = (SaveUserResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getUser(), notNullValue());
    assertThat(response.getUser().getId(), notNullValue());
    assertThat(response.getUser(), is(UserService.getById(response.getUser().getId())));
  }

  @Test
  public void removeUser() {
    /* Add test user */
    SaveUserRequest saveRequest = new SaveUserRequest(generateUser());
    SaveUserResponse saveResponse = (SaveUserResponse) webServiceTemplate.marshalSendAndReceive(saveRequest);

    assertThat(saveResponse.getUser(), notNullValue());
    assertThat(saveResponse.getUser().getId(), notNullValue());
    assertThat(saveResponse.getUser(), is(UserService.getById(saveResponse.getUser().getId())));

    /* Remove test user */
    Long id = saveResponse.getUser().getId();

    RemoveUserRequest request = new RemoveUserRequest(id);
    RemoveUserResponse response = (RemoveUserResponse) webServiceTemplate.marshalSendAndReceive(request);

    assertThat(response.getMessage(), is("User removed."));
    assertThat(UserService.getById(id), nullValue());
  }

  private User generateUser() {
    User user = new User() {
      {
        setFirstName(UUID.randomUUID().toString());
        setLastName(UUID.randomUUID().toString());
        setBirthday(LocalDate.now());
        setEmail(UUID.randomUUID().toString() + "@server.com");
        setRoles("ROLE_REGISTERED_USER");
        setPassword(UUID.randomUUID().toString());
        setUserAccount(new UserAccount(BigDecimal.valueOf(new Random().nextDouble())));
      }
    };
    return user;
  }
}
